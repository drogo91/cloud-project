import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthService } from "./shared/services/auth.service";
import { CallbackComponent } from './pages/callback/callback.component';
import {MatCardModule, MatGridListModule, MatTabsModule, MatToolbarModule} from "@angular/material";
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { MyLineChartComponent } from './shared/charts/my-line-chart/my-line-chart.component';
import { MyRadarChartComponent } from './shared/charts/my-radar-chart/my-radar-chart.component';
import { MyPieChartComponent } from './shared/charts/my-pie-chart/my-pie-chart.component';
import { MyBarChartComponent } from './shared/charts/my-bar-chart/my-bar-chart.component';
import { MyDoughnutChartComponent } from './shared/charts/my-doughnut-chart/my-doughnut-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    CallbackComponent,
    MyLineChartComponent,
    MyRadarChartComponent,
    MyPieChartComponent,
    MyBarChartComponent,
    MyDoughnutChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    MatCardModule,
    ChartsModule,
    MatTabsModule,
    MatGridListModule,
  ],
  providers: [
    AuthService,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
