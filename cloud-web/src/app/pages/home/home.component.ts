import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import {GithubData, GithubService, TopPrimaryLanguages, TopRepositorie} from "../../shared/services/github.service";

export interface CodeValue {
  code: string;
  value: number;
}

export interface graph {
  numbers: number[];
  label: string;
}

export interface RadarGraph {
  topPrimaryLanguage: graph;
  topLanguage: graph;
  labels: string[];
}

@Component({
  selector: 'app-profile',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  public profile: any;
  public githubData: GithubData;

  public topPrimaryLanguagesTitle = 'Principales langues primaires';

  public topLanguage = 'Language les plus utilisés';

  public topRepository = 'Top repository';

  public topMemberRepositories = 'Top repository des membres';

  constructor(public authService: AuthService,
              private githubService: GithubService) { }

  ngOnInit() {
    if (this.authService.userProfile) {
      this.profile = this.authService.userProfile;
    } else {
      this.authService.getProfile((err, profile) => {
        this.profile = profile;
      });
    }

    this.githubService.gerGithubData().subscribe((d: GithubData) => {
      this.githubData = d;
    });
  }

  public getAveragePerMember(): number {
    if (this.githubData) {
      return this.githubData.totalRepositories / this.githubData.members;
    }

    return 0;
  }

  public getDataLineChartTopPrimaryLanguages() {
    let codeValues: CodeValue[] = [];
    this.githubData.topPrimaryLanguages.forEach((topPrimaryLanguage: TopPrimaryLanguages) => {
      codeValues.push({ code: topPrimaryLanguage.language, value: topPrimaryLanguage.count } as CodeValue)
    });

    return codeValues;
  }

  public getDataLineChartTopLanguages() {
    let codeValues: CodeValue[] = [];
    this.githubData.topLanguages.forEach((topPrimaryLanguage: TopPrimaryLanguages) => {
      codeValues.push({ code: topPrimaryLanguage.language, value: topPrimaryLanguage.count } as CodeValue)
    });

    return codeValues;
  }

  public getDataLineChartTopRepositories() {
    let codeValues: CodeValue[] = [];
    this.githubData.topRepositories.forEach((topRepositorie: TopRepositorie) => {
      codeValues.push({ code: topRepositorie.repo, value: topRepositorie.count } as CodeValue)
    });

    return codeValues;
  }

  public getDataLineChartTopMemberRepositories() {
    let codeValues: CodeValue[] = [];
    this.githubData.topMemberRepositories.forEach((topMemberRepositorie: TopRepositorie) => {
      codeValues.push({ code: topMemberRepositorie.repo, value: topMemberRepositorie.count } as CodeValue)
    });

    return codeValues;
  }

  public getDataRadarChartTopLanguage(): RadarGraph {
    let topPrimaryLanguage: graph = { numbers: [], label: '' };
    let topLanguage: graph = { numbers: [], label: '' };
    let labels: string[] = [];
    let radarGraph: RadarGraph = { labels: [], topLanguage: { numbers: [], label: '' }, topPrimaryLanguage: { numbers: [], label: '' } };

    if (this.githubData) {
      console.log('---------');
      console.log(this.githubData);
      console.log('---------');
      this.githubData.topLanguages.forEach((language: TopPrimaryLanguages) => {
        if (labels.filter((label: string) => label === language.language).length === 0) {
          labels.push(language.language);
        }
      });

      this.githubData.topPrimaryLanguages.forEach((language: TopPrimaryLanguages) => {
        if (labels.filter((label: string) => label === language.language).length === 0) {
          labels.push(language.language);
        }
      });

      console.log('----- label ----');
      console.log(labels);
      console.log('----- label ----');

      labels.forEach((label: string) => {
        this.githubData.topLanguages.forEach((language: TopPrimaryLanguages) => {
          if (label === language.language) {
            topLanguage.numbers.push(language.count);
          }
        });

        this.githubData.topPrimaryLanguages.forEach((language: TopPrimaryLanguages) => {
          if (label === language.language) {
            topPrimaryLanguage.numbers.push(language.count);
          }
        });
      });
    }

    console.log(topLanguage);
    console.log(topPrimaryLanguage);

    radarGraph.labels = labels;
    radarGraph.topLanguage = topLanguage;
    radarGraph.topPrimaryLanguage = topPrimaryLanguage;

    return radarGraph;
  }

}
