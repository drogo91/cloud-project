import {Component, OnInit} from '@angular/core';
import { LoginService } from "../../shared/services/login.service";
import {tap} from "rxjs/operators";
import {AuthService} from "../../shared/services/auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(public authService: AuthService) { }

}
