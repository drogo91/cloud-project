import {Component, Input, OnInit} from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import {CodeValue} from "../../../pages/home/home.component";

@Component({
  selector: 'app-my-line-chart',
  templateUrl: './my-line-chart.component.html',
  styleUrls: ['./my-line-chart.component.css']
})
export class MyLineChartComponent implements OnInit {

  @Input()
  public codeValues: CodeValue[];

  @Input()
  public title: string;

  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  constructor() { }

  ngOnInit() {
    this._initLineChartData();
    this._lineChartLabels();
  }

  private _lineChartLabels = (): void => {
    let labels: string[] = [];

    if (this.codeValues) {
      this.codeValues.forEach((codeValue: CodeValue) => {
        labels.push(codeValue.code);
      })
    }

    this.lineChartLabels = labels;
  };

  private _initLineChartData = (): void => {
    let values: number[] = [];

    if (this.codeValues) {
      this.codeValues.forEach((codeValue: CodeValue) => {
        values.push(codeValue.value);
      })
    }

    this.lineChartData = [
      { data: values, label: this.title },
    ];
  };

}
