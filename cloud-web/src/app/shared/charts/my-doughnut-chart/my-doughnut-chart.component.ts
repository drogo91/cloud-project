import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { Label, MultiDataSet } from 'ng2-charts';

@Component({
  selector: 'app-my-doughnut-chart',
  templateUrl: './my-doughnut-chart.component.html',
  styleUrls: ['./my-doughnut-chart.component.css']
})
export class MyDoughnutChartComponent implements OnInit {

  public doughnutChartLabels: Label[] = ['Swiip', 'fhussonnois', 'bpetetot', 'frinyvonnick', 'hgwood'];
  public doughnutChartData: MultiDataSet = [
    [3850, 208, 49, 37, 33],
    [69, 39, 24],
    [47],
    [30],
    [33],
  ];
  public doughnutChartType: ChartType = 'doughnut';

  constructor() { }

  ngOnInit() {
  }

}
