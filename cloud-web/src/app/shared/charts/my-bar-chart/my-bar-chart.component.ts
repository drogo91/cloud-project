import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-my-bar-chart',
  templateUrl: './my-bar-chart.component.html',
  styleUrls: ['./my-bar-chart.component.css']
})
export class MyBarChartComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['alpine-chrome', 'angular-from-scratch', 'Blogs', 'zenscaler', 'k8s-on-gce', 'kafka-specs', 'MARCEL', 'alpine-node', 'goru', 'java-snapshot-matcher'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [211, 39, 20, 19, 15, 13, 11, 10, 10, 9], label: 'Top Repository' },
  ];

  constructor() { }

  ngOnInit() {
  }

}
