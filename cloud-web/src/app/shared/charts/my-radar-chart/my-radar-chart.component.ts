import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-my-radar-chart',
  templateUrl: './my-radar-chart.component.html',
  styleUrls: ['./my-radar-chart.component.css']
})
export class MyRadarChartComponent implements OnInit {

  public radarChartOptions: ChartOptions = {
    responsive: true,
  };
  public radarChartLabels: Label[] = ['JavaScript', 'Java', 'HTML', 'Shell', 'Go', 'Dockerfile', 'TypeScript', 'CSS', 'Kotlin', 'Vue', 'Python', 'C++', 'PHP',];

  public radarChartData: ChartDataSets[] = [
    { data: [58, 50, 27, 8, 7, 7, 6, 5, 5, 4, 0, 0, 0], label: 'Principales langues primaire' },
    { data: [250, 116, 34, 0, 9, 0, 20, 15, 0, 0, 16, 11, 11], label: 'Languages les plus utilisés' }
  ];
  public radarChartType: ChartType = 'radar';

  constructor() { }

  ngOnInit() {
  }

}
