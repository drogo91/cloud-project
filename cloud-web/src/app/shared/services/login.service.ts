import { Injectable } from '@angular/core';
import {catchError} from "rxjs/operators";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private handleError: boolean;

  constructor(private http: HttpClient) { }
}
