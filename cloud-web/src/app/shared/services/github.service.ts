import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import { catchError, map, tap } from 'rxjs/operators';

export interface GithubData {
  organization: string,
  members: number,
  membersWithRepositories: number,
  topPrimaryLanguages: TopPrimaryLanguages[],
  topRepositories: TopRepositorie[],
  totalRepositories: number,
  topLanguages: TopPrimaryLanguages[],
  topMemberRepositories: TopRepositorie[],
}

export interface TopPrimaryLanguages {
  language: string,
  count: number,
}

export interface TopRepositorie {
  repo: string,
  count: number,
}

@Injectable({ providedIn: 'root' })
export class MessageService {
  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  constructor(private httpClient: HttpClient,
              private messageService: MessageService) { }

  public gerGithubData (): Observable<GithubData> {
    return this.httpClient.get<GithubData>('http://localhost:8080/stats')
      .pipe(
        catchError(this.handleError<GithubData>('getGithubData', ))
      );
  }

  // organization

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }
}
